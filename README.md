# High Five!
An application supporting an Employee Recognition program workflow

[![pipeline status](https://gitlab.com/ucsdlibrary/development/highfive/badges/trunk/pipeline.svg)](https://gitlab.com/ucsdlibrary/development/highfive/-/commits/trunk)
[![coverage report](https://gitlab.com/ucsdlibrary/development/highfive/badges/trunk/coverage.svg)](https://gitlab.com/ucsdlibrary/development/highfive/-/commits/trunk)

## Local Development
### Email / Letter Opener
To view emails submitted in a development context, you can navigate to:
```
http://localhost:3000/letter_opener
```

### Environment Variables
This app is using ENV-driven configuration options through
[dotenv](https://github.com/bkeepers/dotenv) for non-production environments.

To override environment variables set by `.env`, create a `.env.local` file: <https://github.com/bkeepers/dotenv#what-other-env-files-can-i-use>.  We recommend adding the private variables into the `.env.local` file to avoid the risk of accidentally committing an updated file that contains the password.

### Docker
1. Install docker and docker-compose
1. To spin up your environment, run `make up`
    - Run `make menu` to see all options

> If you wish to persist your local development data across sessions, you can
temporarily change the `DATABASE_COMMAND` to something like `db:migrate`

### OpenTelemetry

The docker-compose development environment adds a [jaeger][jaeger] service which acts as a local [open telemetry][otel]
collector and UI.

It will be accessible at: `http://localhost:16686/` and can be used to check trace, span, event, and attribute
information locally.

#### Seeding Data

To load a backup copy of the latest production application data locally, after running the docker commands above to
start up the local environment, you can run the `db:seed` rake command as follows:

You can use the Makefile:

`make seed`

Or you can invoke directly (development example):

`docker-compose exec web rake db:seed`

Note that you will need to either be on campus or using the VPN, as this accesses LDAP data from Active Directory via
[lib-adbridge][lib-adbridge], as well as Minio/S3 for the data backup. You will need the following environment variables
added to your `.env.local` file.  This info can be found in the [config][config] project.

- AWS_ACCESS_KEY_ID
- AWS_SECRET_ACCESS_KEY
- ADBRIDGE_USERNAME
- ADBRIDGE_PASSWORD

#### Testing
1. For full test suite, run `make test`
1. Run individual tests with `docker-compose exec web` prefix for any RSpec command.

Examples:
```
docker-compose exec web bin/rails spec
docker-compose exec web bundle exec rake spec
docker-compose exec web bundle exec rake spec/models/user.rb
docker-compose exec web bundle exec rake spec/models/user.rb:50
...
```

### Troubleshooting
If you get odd errors about tmp or cached file permissions, particularly if you
are testing both the development and production docker environments, you should
try removing the cached files in the `./tmp` folder.

`sudo rm -rf ./tmp/cache`

#### JavaScript
The [`webpack-bundle-analyzer` plugin][webpack-analyzer] is installed as a dev dependency.

Once you `make up` you can navigate to `http://localhost:8888` to view the UI

#### Debugging
With docker-compose running, in a new terminal/tab attach to the container:
`docker attach hifive_web_1`

The [`debug` gem][debug] gem is installed as a dev dependency. To use it, you
can require the library and then create a breakpoint as follows:

```ruby
def my_method
  require 'debug'
  binding.break
  # some code to debug
end
```

### Production Docker
The main `Dockerfile` is setup for production. If you want to run this locally,
you can use the `./bin/docker-helper.sh -p` script.

Alternatively, you can directly deploy the High Five helm chart. See below.

Note you _cannot_ run the test suite in this environment. The tooling is not in place.

## Deploying with GitLab CI/CD & Helm
The application currently uses the [gitlab-ci-tools][gitlab-ci-tools] templates
for deploying and managing `review` and `staging` applications.

### Values Files
The helm chart deployments for CI expect base64-encoded values `yaml` files. If
you need to update or reference these files, you may copy them from GitLab and
`base64 -d` them. Or, you can access them in LastPass via the entry `HighFive -
helm values files`.

*Note*: Please make sure any changes are reflect in both the CI/CD variable(s) and
the LastPass entry!

### Review Apps
A successful Merge Request pipeline will result in a Helm deployment to our
Rancher cluster with an application dedicated to the code as it exists in the
Merge Request. This environment:

* Will have all current production data (as of the previous day) loaded into it
* Will use the `production` `Dockerfile` target, exactly as the `production` and
    `staging` environments
* Will use the same helm chart as the `production` and `staging` environments.

Differences in Review Apps:
* Authentication uses local `developer` auth. It is (currently) not possible to
    dynamically generate Google Auth credentials for a Review App wildcard-style
    DNS entry.
* Will not use TLS cert for SSL termination for ingress. This app will run over `http`.

#### Cleanup
Upon a successful merge (or close) of a Merge Request the Review App will automatically
run a cleanup job.

Alternatively, if the Merge Request lasts longer than a week, the Review App will be
automatically removed from Rancher/k8s.

### Staging
A successful `trunk` pipeline run will result in a Helm deployment to our
Rancher cluster, within the `highfive-staging` namespace.

This deployment is identical to Production and only differs in the environment
variable values for things like TLS cert, ingress URL, and database credentials.

### Production
A successful `trunk` pipeline run will result in a `manual` `production_deploy`
job creation. This allows selective `production` environment Helm deployments to
our Rancher cluster, within the `highfive-production` namespace.

This deployment is identical to Production and only differs in the environment
variable values for things like TLS cert, ingress URL, and database credentials.

### Rollback
A `manual` `rollback` job is created in case something goes wrong in the
deployment to `staging` or `production`. The provides a quick rollback option via the GitLab UI.


[config]:https://gitlab.com/ucsdlibrary/devops/config
[debug]: https://github.com/ruby/debug
[jaeger]:https://www.jaegertracing.io/
[lib-adbridge]: https://gitlab.com/ucsdlibrary/development/lib-adbridge
[otel]:https://opentelemetry.io/
