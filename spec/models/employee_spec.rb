require "rails_helper"

RSpec.describe Employee, type: :model do
  context "invalid recognition" do
    let(:employee) { Employee.new }
    it "must have required attributes" do
      expect(employee).to be_invalid
    end
  end

  context "valid recognition" do
    let(:employee) { FactoryBot.build_stubbed(:employee) }
    let(:employee_without_manager) { FactoryBot.build_stubbed(:employee_without_manager) }
    it "persists with required attributes" do
      expect(employee).to be_valid
    end

    it "persists without requiring a manager" do
      expect(employee_without_manager).to be_valid
    end
  end

  describe ".needs_update?" do
    let(:entry) { JSON.load_file(File.join(fixture_paths.first, "single_employee_record.json")) }

    it "returns true with a new record" do
      e = Employee.new
      expect(described_class.needs_update?(e, entry["ldapChanged"])).to be true
    end

    it "returns true with an outdated record" do
      e = FactoryBot.build_stubbed(:employee, updated_at: Time.parse("2018-11-01"))
      expect(described_class.needs_update?(e, entry["ldapChanged"])).to be true
    end

    it "returns false with an up to date record" do
      e = FactoryBot.build_stubbed(:employee, updated_at: Time.parse("2018-11-30"))
      expect(described_class.needs_update?(e, entry["ldapChanged"])).to be false
    end

    it "returns true when an employee needs re-activation" do
      e = FactoryBot.build_stubbed(:employee, active: false)
      expect(described_class.needs_update?(e, entry["ldapChanged"])).to be true
    end
  end

  describe ".active_employees" do
    let!(:active_employee) { FactoryBot.create(:employee, active: true, display_name: "active employee") }
    let!(:inactive_employee) { FactoryBot.create(:employee, active: false, display_name: "inactive employee") }
    it "only includes currently active employees" do
      expect(described_class.active_employees.map(&:display_name)).to eq(["active employee"])
    end
  end

  describe ".update_status_for_all" do
    it "deactives employees who are no longer in ldap OU" do
      active_employee1 = FactoryBot.create(:employee, active: true, display_name: "active employee")
      active_employee2 = FactoryBot.create(:employee, active: true, display_name: "active employee 2")
      employee_to_deactivate = FactoryBot.create(:employee, active: true, display_name: "inactive employee")
      active_employees = [active_employee1.uid, active_employee2.uid]
      described_class.update_status_for_all(active_employees)
      expect(Rails.logger).to_not receive(:error).with(employee_to_deactivate)
      expect(described_class.active_employees.map(&:display_name)).to eq(["active employee", "active employee 2"])
    end

    it "does not try deactivating already deactivated employees" do
      active_employee = FactoryBot.create(:employee, active: true, display_name: "active employee")
      deactivated_employee = FactoryBot.create(:employee, active: false, display_name: "inactive employee")
      active_employees = [active_employee.uid]

      expect(Rails.logger).to_not receive(:error).with(deactivated_employee)
      described_class.update_status_for_all(active_employees)
    end
  end

  describe ".populate_from_adbridge" do
    let(:entry1) { JSON.load_file(File.join(fixture_paths.first, "single_employee_record.json")) }
    it "should handle updating Employee records", :aggregate_failures do
      expect(Employee.count).to be_zero
      # ensure the updated_at date is prior to our record to update
      travel_to Date.new(2018, 10, 0o2) do
        described_class.populate_from_adbridge(entry1)
      end

      entry1["displayName"] = "Batman"
      entry1["ldapChanged"] = ["20181130172427.0Z"] # newer record simulation
      described_class.populate_from_adbridge(entry1)
      expect(Employee.count).to be(1)
      expect(Employee.first.display_name).to eq("Batman")
      expect(Employee.first.email).to eq("aemployee@ucsd.edu")
    end
  end

  describe ".populate_from_adbridge for employee without a mail attribute" do
    let(:entry1) { JSON.load_file(File.join(fixture_paths.first, "single_employee_record.json")) }
    it "should not handle updating Employee records", :aggregate_failures do
      # remove mail entry from fixture
      entry1["userPrincipalName"] = ""

      expect(Employee.count).to be_zero
      # ensure the updated_at date is prior to our record to update
      travel_to Date.new(2018, 10, 0o2) do
        described_class.populate_from_adbridge(entry1)
      end

      expect(Employee.count).to be_zero
    end
  end
end
