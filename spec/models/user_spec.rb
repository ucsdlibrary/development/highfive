require "rails_helper"

RSpec.describe User, type: :model do
  let(:auth_hash_google) {
    OmniAuth::AuthHash.new({
      provider: "google_oauth2",
      uid: "1",
      info: {"email" => "test@ucsd.edu", "name" => "Dr. Seuss"}
    })
  }

  let(:auth_hash_developer) { {email: "developer@ucsd.edu"} }

  let(:invalid_auth_hash_missing_info) {
    OmniAuth::AuthHash.new({
      provider: "google_oauth2",
      uid: "test"
    })
  }
  describe "#developer?" do
    it "should return true when provider is developer" do
      user = described_class.find_or_create_for_developer(auth_hash_developer)
      expect(user.developer?).to be true
    end

    it "should return false when provider is google_oauth2" do
      user = described_class.find_or_create_for_google_oauth2(auth_hash_google)
      expect(user.developer?).to be false
    end
  end

  describe ".find_or_create_for_developer" do
    it "should create a User for first time user" do
      user = described_class.find_or_create_for_developer(auth_hash_developer)

      expect(user).to be_persisted
      expect(user.provider).to eq("developer")
      expect(user.uid).to eq("developer@ucsd.edu")
      expect(user.email).to eq("developer@ucsd.edu")
      expect(user.full_name).to eq("developer")
    end

    it "should reuse an existing User if the access token matches" do
      described_class.find_or_create_for_developer(auth_hash_developer)
      described_class.find_or_create_for_developer(auth_hash_developer)

      expect(described_class.count).to be(1)
    end
  end

  describe ".find_or_create_for_google_oauth2" do
    it "should create a User when a user is first authenticated" do
      user = described_class.find_or_create_for_google_oauth2(auth_hash_google)
      expect(user).to be_persisted
      expect(user.provider).to eq("google_oauth2")
      expect(user.uid).to eq("1")
      expect(user.email).to eq("test@ucsd.edu")
      expect(user.full_name).to eq("Dr. Seuss")
    end

    it "should not persist a shib response with bad or missing information" do
      described_class.find_or_create_for_google_oauth2(invalid_auth_hash_missing_info)
      expect(described_class.find_by(uid: "test", provider: "google_oauth2")).to be nil
    end
  end

  describe ".administrator?" do
    context "user is not in group" do
      it "returns false" do
        user = described_class.find_or_create_for_google_oauth2(auth_hash_google)
        allow(AdBridge).to receive(:in_highfive_group?).and_return(false)
        expect(described_class.administrator?(user.uid)).to be false
      end
    end
    context "user is in group" do
      it "returns true" do
        user = described_class.find_or_create_for_google_oauth2(auth_hash_google)
        allow(AdBridge).to receive(:in_highfive_group?).and_return(true)
        expect(described_class.administrator?(user.uid)).to be true
      end
    end
  end

  describe ".employee_uid" do
    context "when google-oauth uid is a number" do
      before(:all) do
        @employee = Employee.create(email: "test@ucsd.edu",
          uid: "test", name: "Dr. Seuss", display_name: "Dr.", manager: "manager1")
        @employee.save!
      end
      after(:all) do
        @employee.delete
      end

      it "converts google-oauth uid to AD uid" do
        uid = described_class.employee_uid(auth_hash_google.info.email, auth_hash_google.uid)
        expect(uid).to eq(@employee.uid)
      end
    end

    context "with active and inactive employee records for the same person" do
      let(:auth_hash_mack) {
        OmniAuth::AuthHash.new({
          provider: "google_oauth2",
          uid: "1",
          info: {"email" => "amchambe@ucsd.edu", "name" => "New Mack"}
        })
      }
      before(:all) do
        @inactive_employee = Employee.create(active: false, email: "amchambers@ucsd.edu",
          uid: "amchambers", name: "Old Mack", display_name: "Mack", manager: "michael")
        @inactive_employee.save!
        @active_employee = Employee.create(email: "amchambe@ucsd.edu",
          uid: "amchambe", name: "New Mack", display_name: "Mack", manager: "michael")
        @active_employee.save!
      end
      after(:all) do
        @inactive_employee.delete
        @active_employee.delete
      end

      it "should only return active employee" do
        uid = described_class.employee_uid(auth_hash_mack.info.email, auth_hash_mack.uid)
        expect(uid).to eq(@active_employee.uid)
      end
    end

    context "when google-oath email does not match AD email" do
      before(:all) do
        @employee = Employee.create(email: "test@AD.UCSD.EDU",
          uid: "test", name: "Dr. Seuss", display_name: "Dr.", manager: "manager1")
        @employee.save!
      end
      after(:all) do
        @employee.delete
      end

      it "only needs to match the email account name" do
        uid = described_class.employee_uid(auth_hash_google.info.email, auth_hash_google.uid)
        expect(uid).to eq(@employee.uid)
      end
    end
  end
end
