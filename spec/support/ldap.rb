def mock_employee_query
  FactoryBot.create(:employee, display_name: "Employee, Joe")
  FactoryBot.create(:employee, display_name: "Employee, Jane")
end

def mock_valid_library_employee
  allow(AdBridge).to receive(:library_staff?).and_return(true)
end

def mock_invalid_library_employee
  allow(AdBridge).to receive(:library_staff?).and_return(false)
end
