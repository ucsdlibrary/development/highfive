require "rails_helper"

RSpec.describe AdBridge, type: :service do
  describe ".in_highfive_group?" do
    context "with user in the group" do
      before do
        allow(described_class).to receive(:query).and_return("true")
      end
      it "returns true" do
        expect(described_class.in_highfive_group?("adminuser")).to eq(true)
      end
    end
    context "with user not in the group" do
      before do
        allow(described_class).to receive(:query).and_return("false")
      end
      it "returns false" do
        expect(described_class.in_highfive_group?("notadmin")).to eq(false)
      end
    end
  end
  describe ".library_staff??" do
    context "with a library staff member" do
      before do
        allow(described_class).to receive(:query).and_return("true")
      end
      it "returns true" do
        expect(described_class.library_staff?("staffmember")).to eq(true)
      end
    end
    context "with a non-library staff user" do
      before do
        allow(described_class).to receive(:query).and_return("false")
      end
      it "returns false" do
        expect(described_class.library_staff?("notstaff")).to eq(false)
      end
    end
  end
  describe ".employees" do
    let(:employee_response) { JSON.load_file(File.join(fixture_paths.first, "employees.json")) }
    before do
      allow(described_class).to receive(:query).and_return(JSON.dump(employee_response))
    end
    it "returns true" do
      described_class.employees
      expect(Employee.count).to eq(2)
    end
  end
end
