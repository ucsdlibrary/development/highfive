require "rails_helper"

RSpec.describe SlackNotifier, type: :service do
  describe "#call" do
    let!(:notifier) {
      described_class.new(employee_name: "Triton, Jane",
        employee_rec_url: "http://example.com/recognitions/1")
    }
    # Temporarily use SlackNotifier in test environment
    # This is set to false in config/environments/test
    # to prevent accidentally spamming the Slack channel during testing
    before(:each) do
      Rails.application.config.send_slack_notifications = true
    end

    after(:each) do
      Rails.application.config.send_slack_notifications = false
    end

    it "prints first name then last when name is of form lastname, firstname" do
      expect(notifier.employee_name).to eq("Jane Triton")
    end

    context "with SLACK_WEBHOOK_URL set" do
      before do
        allow(notifier).to receive(:slack_webhook_url).and_return("http://webhook.slack.com")
        allow(notifier).to receive(:notify_slack).and_return(0)
      end

      it "calls notify_slack" do
        expect(notifier.call).to be_truthy
      end
    end

    context "without SLACK_WEBHOOK_URL set" do
      before do
        allow(ENV).to receive(:[]).with("SLACK_WEBHOOK_URL").and_return(nil)
      end

      it "returns and does not post a notification" do
        expect(notifier.call).to be(nil)
      end
    end
  end
end
