# Work around https://github.com/hotwired/turbo-rails/issues/512
# TODO: remove this file once fixed
Rails.autoloaders.once.do_not_eager_load("#{Turbo::Engine.root}/app/channels")
