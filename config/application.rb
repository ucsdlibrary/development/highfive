require_relative "boot"

require "rails"
require "active_model/railtie"
require "active_job/railtie"
require "active_record/railtie"
require "action_controller/railtie"
require "action_mailer/railtie"
require "action_view/railtie"
require "sprockets/railtie"

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Highfive
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 7.1
    config.generators.system_tests = nil
    # Default to supporting RAILS_LOG_TO_STDOUT
    config.log_formatter = ::Logger::Formatter.new
    if ENV["RAILS_LOG_TO_STDOUT"].present?
      logger = ActiveSupport::Logger.new($stdout)
      logger.formatter = config.log_formatter
      config.logger = ActiveSupport::TaggedLogging.new(logger)
    end

    config.send_slack_notifications = true

    config.autoload_lib(ignore: %w[assets tasks])

    # Setup email defaults for ActionMailer
    config.action_mailer.delivery_method = ENV.fetch("EMAIL_DELIVERY_METHOD").to_sym
    config.action_mailer.default_url_options = {host: ENV.fetch("APP_HOST")}

    if ENV["EMAIL_SMTP_HOST"].present? && ENV["EMAIL_SMTP_PORT"].present?
      config.action_mailer.smtp_settings = {address: ENV.fetch("EMAIL_SMTP_HOST"),
                                             port: ENV.fetch("EMAIL_SMTP_PORT")}
    end
    # Configuration for the application, engines, and railties goes here.
    #
    # These settings can be overridden in specific environments using the files
    # in config/environments, which are processed later.
    #
    # config.time_zone = "Central Time (US & Canada)"
    # config.eager_load_paths << Rails.root.join("extras")
  end
end
