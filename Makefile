# Makefile for common High Five! development tasks

menu:
	@echo 'build: Run docker-compose build for the current ENV (development, production)'
	@echo 'clean: Run docker-compose down -v for the current ENV (development, production)'
	@echo 'console: Run rails console for debugging and local development'
	@echo 'ctags: Copy gems to vendor/ on host. and generate ctags for local development'
	@echo 'js_upgrade: Upgrade all JavaScript dependencies'
	@echo 'ldap: Run rake task to populate the application with employee ldap data'
	@echo 'lint: Run rubocop to lint all files in the project'
	@echo 'seed: Run docker-compose up for the development environment'
	@echo 'test: Run full rspec test suite using RAILS_ENV=test'
	@echo 'up: Run docker-compose up for the current ENV (development, production)'

build:
	@echo 'Building docker-compose environment'
	@docker-compose build

clean:
	@echo 'Cleaning out docker-compose environment'
	@docker-compose down -v

console:
	@echo 'Running rails console'
	@docker-compose exec web bundle exec rails console

js_upgrade:
	@echo 'Upgrade JavaScript dependencies'
	@docker-compose exec web yarn global add npm-check-updates
	@docker-compose exec web ncu -u
	@docker-compose exec web yarn install --check-files

lint:
	@echo 'Running Standard RB'
	@docker-compose exec web bundle exec standardrb

lint-fix:
	@echo 'Running Standard RB with --fix'
	@docker-compose exec web bundle exec standardrb --fix

ldap:
	@echo 'Populating employee LDAP data from AdBridge'
	@docker-compose exec web bundle exec rake nightly:employees

seed:
	@echo 'Seeding database with sample data'
	@docker-compose exec web bundle exec rake db:seed

test:
	@echo 'Running full test suite'
	@docker-compose exec web bundle exec rake spec

up:
	@echo 'Bringing up docker-compose environment'
	@docker-compose up
