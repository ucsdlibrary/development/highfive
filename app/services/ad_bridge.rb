# frozen_string_literal: true

module AdBridge
  # Given a UID/SAMAccountName determine whether a user is in the highfive admin group
  # @param uid [String] the user id to check. e.g. 'drseuss'
  # @return [Boolean] true/false response indicating if user is in ADBRIDGE_HIGHFIVE_GROUP
  def self.in_highfive_group?(uid)
    group = ENV.fetch("ADBRIDGE_HIGHFIVE_GROUP")
    route = "/GroupMembers/#{group}/#{uid}"

    result = query(route)

    Rails.logger.info("AdBridge.#{__method__} with uid #{uid} response : #{result}")

    ActiveModel::Type::Boolean.new.cast(result)
  rescue => e
    Rails.logger.error("AdBridge.#{__method__} with uid #{uid} error #{e} ")
    false
  end

  def self.library_staff?(uid)
    group = ERB::Util.url_encode("All Library Staff")
    route = "/GroupMembers/#{group}/#{uid}"

    result = query(route)

    Rails.logger.info("AdBridge.#{__method__} with uid #{uid} response : #{result}")

    ActiveModel::Type::Boolean.new.cast(result)
  rescue => e
    Rails.logger.error("AdBridge.#{__method__} with uid #{uid} error #{e} ")
    false
  end

  def self.employees
    Rails.logger.tagged("rake", "employees") { Rails.logger.info "Starting AdBridge Employee load.." }
    group = ERB::Util.url_encode("All Library Staff")
    route = "/GroupMembers/#{group}"

    result = JSON.parse(query(route))

    current_employees = []
    result.each do |employee|
      Employee.populate_from_adbridge(employee)
      current_employees << employee["samAccountName"]
    end
    Employee.update_status_for_all(current_employees)
    Rails.logger.tagged("rake", "employees") { Rails.logger.info "Finished AdBridge Employee load.." }
  rescue => e
    Rails.logger.error("AdBridge.#{__method__} with error #{e} ")
    false
  end

  # Execute an ADBridge query/route
  # @return [String] The response body from the ADBridge query
  def self.query(route)
    adbridge_uri = ENV.fetch("ADBRIDGE_URL")
    uri = URI("#{adbridge_uri}#{route}")
    req = Net::HTTP::Get.new(uri)
    req.basic_auth(ENV.fetch("ADBRIDGE_USERNAME"),
      ENV.fetch("ADBRIDGE_PASSWORD"))

    res = Net::HTTP.start(uri.hostname, uri.port, use_ssl: uri.scheme == "https") { |http|
      http.request(req)
    }

    if res.is_a?(Net::HTTPSuccess)
      res.body
    else
      raise StandardError.new("AdBridge.query with #{route} did not succeed. response code #{res.code}")
    end
  end
end
