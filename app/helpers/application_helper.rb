# frozen_string_literal: true

module ApplicationHelper
  def app_title
    "High Five!"
  end
end
