# frozen_string_literal: true

# User - authenticated Library user
class User < ApplicationRecord
  has_many :recognitions, dependent: :destroy

  def self.find_or_create_for_developer(access_token)
    User.find_by(uid: access_token[:email], provider: "developer") ||
      User.create(uid: access_token[:email],
        provider: "developer",
        email: access_token[:email],
        full_name: "developer")
  end

  # Finds or creates a google oauth2 account
  # @param access_token [OmniAuth::AuthHash]
  def self.find_or_create_for_google_oauth2(access_token)
    logger.tagged("google_oauth2", "access token") do
      logger.info "uid: #{access_token.uid}"
    end
    begin
      email = access_token.info.email || "#{uid}@ucsd.edu"
      uid = employee_uid(email, access_token.uid)
      provider = access_token.provider
      name = access_token.info.name
    rescue => e
      logger.tagged("google_oauth2") { logger.error e } && return
    end
    logger.tagged("google_oauth2", "login") do
      logger.info "email: #{email} uid: #{uid} provider: #{provider} name: #{name}"
    end
    User.find_by(uid: uid) ||
      User.create(uid: uid, provider: provider, email: email, full_name: name)
  end
  # rubocop:enable Metrics/MethodLength, Metrics/AbcSize

  # Check email address for existing Employee, if the UID returned from GAuth is numberic
  def self.employee_uid(user_email, uid)
    return uid unless numeric?(uid)
    # We have inconsistent email addresses, so just check email username
    # staff@AD.UCSD.EDU vs staff@ucsd.edu vs STaff@UCSD.edu (sigh)
    email_account = user_email.to_s.split("@").first
    # Use PostgreSQL case-insensitive ILIKE
    employee = Employee.active_employees.find_by("email ILIKE ?", "#{sanitize_sql_like(email_account)}%")
    return employee.uid.downcase if employee

    uid
  end

  def self.numeric?(uid)
    true if Integer(uid)
  rescue
    false
  end

  def self.administrator?(uid)
    AdBridge.in_highfive_group?(uid)
  end

  def developer?
    provider.eql?("developer")
  end

  # Determine whether the authenticated Shib user is Library staff
  # @param uid [String] example: 'drseuss'
  # @return [Boolean]
  def self.library_staff?(uid)
    AdBridge.library_staff?(uid)
  end
end
