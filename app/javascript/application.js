// Configure your import map in config/importmap.rb. Read more: https://github.com/rails/importmap-rails
//= javascript_include_tag :application
import * as bootstrap from "bootstrap"
import "@hotwired/turbo-rails"

import { Application } from "@hotwired/stimulus"

const application = Application.start()

// Configure Stimulus development experience
application.debug = false
window.Stimulus   = application

export { application }