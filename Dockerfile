ARG IMAGE_REGISTRY='docker.io/'
FROM ${IMAGE_REGISTRY}ruby:3.3.2-alpine3.19 as development

RUN apk --no-cache upgrade && \
  apk add --no-cache \
  build-base \
  gcompat \
  less \
  libxml2-dev \
  libxslt-dev \
  postgresql-dev \
  nodejs \
  npm \
  tzdata \
  shared-mime-info \
  vim \
  yarn \
  && rm -rf /var/cache/apk/*

WORKDIR /app

ENV EDITOR=vim
ENV RAILS_ENV=development
ENV RACK_ENV=development
ENV RAILS_LOG_TO_STDOUT=true
ENV RAILS_ROOT=/app
ENV LANG=C.UTF-8


# Install yarn packages
COPY package.json yarn.lock /app/
RUN yarn install --frozen-lockfile

# Install gems
COPY Gemfile* /app/
RUN bundle config build.nokogiri --use-system-libraries && \
  gem update bundler && \
  bundle install --jobs "$(nproc)" --retry 2

COPY . /app

CMD ["bundle", "exec", "puma", "-b", "tcp://0.0.0.0:3000"]
ENTRYPOINT ["/bin/sh", "/app/docker/docker-entrypoint.sh"]

FROM ${IMAGE_REGISTRY}ruby:3.3.2-alpine3.19 as production

RUN apk --no-cache upgrade && \
  apk add --no-cache \
  libxml2-dev \
  libxslt-dev \
  gcompat \
  postgresql-dev \
  nodejs \
  shared-mime-info \
  tzdata \
  yarn \
  && rm -rf /var/cache/apk/*

WORKDIR /app

ENV APP_HOST=localhost:3000
ENV EMAIL_DELIVERY_METHOD=letter_opener_web
ENV LANG=C.UTF-8
ENV RACK_ENV=production
ENV RAILS_ENV=production
ENV RAILS_LOG_TO_STDOUT=true
ENV RAILS_ROOT=/app
ENV RAILS_SERVE_STATIC_FILES=true
ENV SECRET_KEY_BASE=something

COPY --from=development /app ./
COPY --from=development /usr/local/bundle /usr/local/bundle

RUN bundle config set without 'development test' \
  && bundle clean --force \
  # Remove unneeded files (cached *.gem, *.o, *.c)
  && find /usr/local/bundle -name "*.gem" -delete \
  && find /usr/local/bundle -name "*.c" -delete \
  && find /usr/local/bundle -name "*.o" -delete

RUN apk add --no-cache yarn && \
  RAILS_ENV=production \
  NODE_ENV=production \
  POSTGRESQL_DATABASE='fake' \
  POSTGRESQL_HOST='fake' \
  POSTGRESQL_USERNAME='fake' \
  POSTGRESQL_PASSWORD='fake' \
  bundle exec rails assets:precompile && \
  apk del yarn

RUN rm -rf node_modules tmp/* log/* vendor/dictionary* app/assets/builds/*

EXPOSE 3000

CMD ["bundle", "exec", "puma", "-b", "tcp://0.0.0.0:3000"]
ENTRYPOINT ["/bin/sh", "/app/docker/docker-entrypoint.sh"]
